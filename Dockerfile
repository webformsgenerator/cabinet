FROM node:16.13.1-alpine as frontend
USER node
RUN mkdir -p /home/node/app
COPY --chown=node:node ./ /home/node/app
RUN rm -rf /home/node/app/node_modules
# RUN chmod -R 0777 /home/node/app
# RUN chown -R node: /app
# RUN rm /frontend/node_modules
WORKDIR /home/node/app
# RUN npm -g config set user root
RUN npm install --quiets
RUN npm run build -- --configuration production --base-href='/cabinet/'

FROM alpine
COPY --from=frontend /home/node/app/dist/cabinet /cabinet
ENTRYPOINT ["sh"]

import {FormElementBase} from "../_components/dynamic-element/form-element-base";
import {ElementText} from "../_components/dynamic-element/element-text";
import {ElementNumber} from "../_components/dynamic-element/element-number";
import {ElementCheckbox} from "../_components/dynamic-element/element-checkbox";
import {ElementTextArea} from "../_components/dynamic-element/element-textarea";
import {ElementSelectNumRangeCondition} from "../_components/dynamic-element/element-select-num-range-condition";
import {ElementDropdown} from "../_components/dynamic-element/element-dropdown";


export class SurveyBlock {
  show: boolean | undefined;
  order: string[] = [];
  stringList: ElementText[] = [];
  numberList: ElementNumber[] = [];
  checkBoxList: ElementCheckbox[] = [];
  dropDownList: ElementDropdown[] = [];
  geoCoordList: ElementText[] = [];
  textList: ElementTextArea[] = [];
  selectNumRangeConditionList: ElementSelectNumRangeCondition[] = [];


  constructor(instanceData?: SurveyBlock) {
    if (instanceData) {
      this.deserialize(instanceData)
    }
  }

  private deserialize(instanceData: SurveyBlock) {
    console.log(instanceData)
    this.show = instanceData.show
    this.order = instanceData.order
    this.stringList = instanceData.stringList
    this.numberList = instanceData.numberList
    this.checkBoxList = instanceData.checkBoxList
    this.dropDownList = instanceData.dropDownList
    this.geoCoordList = instanceData.geoCoordList
    this.textList = instanceData.textList
    this.selectNumRangeConditionList = instanceData.selectNumRangeConditionList
  }

  addElement(element: FormElementBase<any>) {

    if (element instanceof ElementNumber) {
      this.order.push(element.key)
      this.numberList.push(element)
      return
    }

    if (element instanceof ElementText) {
      this.order.push(element.key)
      this.stringList.push(element)
      return
    }

    if (element instanceof ElementCheckbox) {
      this.order.push(element.key)
      this.checkBoxList.push(element)
      return
    }

    if (element instanceof ElementTextArea) {
      this.order.push(element.key)
      this.textList.push(element)
      return
    }

    if (element instanceof ElementDropdown) {
      this.order.push(element.key)
      this.dropDownList.push(element)
      return
    }

    if (element instanceof ElementSelectNumRangeCondition) {
      this.order.push(element.key)
      this.selectNumRangeConditionList.push(element)
      return
    }

    console.log('Unexpected type of element: ' + element.controlType);
    throw new Error('Unexpected type of element: ' + element.controlType);
  }

  getElementsOrdered(): FormElementBase<any>[] {
    let elements: FormElementBase<any>[] = []
    //console.log('getElementsOrdered')
    //console.log(this)
    // Проходимся по всем элементам в блоке массива
    for (let n = 0; n < this.order.length; n++) {
      elements.push(this.getElementObjectByName(this.order[n]));
    }

    return elements
  }

  getElementObjectByName(elementName: string): FormElementBase<any> {
    // Проходимся по всем типам
    // В каждом типе проверяем наличие имени
    // Если находим то возвращаем найденный элемент
    //console.log('getElementObjectByName');
    //console.log(elementName);

    const elementTypes: Array<keyof SurveyBlock> = ['numberList', 'stringList', 'textList', 'checkBoxList', 'dropDownList', 'geoCoordList', 'selectNumRangeConditionList'];

    if (this.stringList) {
      for (let i = 0; i < this.stringList.length; i++) {
        if (this.stringList[i].key == elementName) {
          return this.stringList[i]
        }
      }
    }


    for (let i = 0; i < this.numberList.length; i++) {
      if (this.numberList[i].key == elementName) {
        return this.numberList[i]
      }
    }

    for (let i = 0; i < this.textList.length; i++) {
      if (this.textList[i].key == elementName) {
        return this.textList[i]
      }
    }

    for (let i = 0; i < this.checkBoxList.length; i++) {
      if (this.checkBoxList[i].key == elementName) {
        return this.checkBoxList[i]
      }
    }

    if (this.dropDownList) {
      for (let i = 0; i < this.dropDownList.length; i++) {
        if (this.dropDownList[i].key == elementName) {
          return this.dropDownList[i]
        }
      }
    }


    for (let i = 0; i < this.stringList.length; i++) {
      if (this.stringList[i].key == elementName) {
        return this.stringList[i]
      }
    }

    for (let i = 0; i < this.geoCoordList.length; i++) {
      if (this.geoCoordList[i].key == elementName) {
        return this.geoCoordList[i]
      }
    }

    for (let i = 0; i < this.selectNumRangeConditionList.length; i++) {
      if (this.selectNumRangeConditionList[i].key == elementName) {
        return this.selectNumRangeConditionList[i]
      }
    }

    // for (let et = 0; et < elementTypes.length; et++) {
    //   let elementType: keyof SurveyBlock = elementTypes[et];
    //   console.log(elementType)
    //   let el = this[elementType]
    //   if (el == undefined) {
    //     continue
    //   }
    //   for (let i = 0; i < el.length; i++) {
    //     console.log(el)
    //     let currEl = el[i]
    //
    //     if (currEl['key'].key == elementName) {
    //       return currEl
    //     }
    //   }
    // }

    // let el = new ElementNumber()
    // el.key = 'test'
    // el.label = 'test'
    //
    // return el

    throw new Error('Unknown element: ' + elementName);

  }

}

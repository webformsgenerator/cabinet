export class User {
  id: number | undefined;
  clientId: number | undefined;
  email: string | undefined;
  lastName: string | undefined;
  firstName: string | undefined;
  secondName: string | undefined;
  login: string | undefined;
  needChangePass: string | undefined;
  accessToken: string | undefined;
  enabled: boolean | undefined;
  refreshToken: string |undefined;
}

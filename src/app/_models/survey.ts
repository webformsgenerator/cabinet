import {SurveyBlock} from "./surveyBlock";
import {FormControl, Validators} from "@angular/forms";

export class Survey {
  name: string | undefined;
  about: string | undefined;
  mode: string | undefined;
  blocks: { [key: string]: SurveyBlock } = {}

  constructor(instanceData?: Survey) {
    if (instanceData) {
      this.deserialize(instanceData)
    }
  }

  private deserialize(instanceData: Survey) {
    this.name = instanceData.name
    this.about = instanceData.about
    this.mode = instanceData.mode

    for (let surveyBlocksKey in instanceData.blocks) {
      console.log(surveyBlocksKey)
      let block = new SurveyBlock(instanceData.blocks[surveyBlocksKey])
      this.blocks[surveyBlocksKey] = block;
    }
  }

  public addBlock() {
    let block = new SurveyBlock()

    // Получаем количество элементов в списке
    let blocksCnt = Object.keys(this.blocks).length;

    // Генерим очередное имя блоку
    let blockName = 'block' + blocksCnt;

    // Добавляем блок под сгенерированным именем
    this.blocks[blockName] = block
  }
}

export class ClientSetting {
  id: Number | undefined;
  codeId: Number = -1;
  value: String = "";
}

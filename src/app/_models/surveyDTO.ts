export class SurveyDTO {
  surveyId: number | undefined;
  uuidId: string | undefined;
  userId: string | undefined;
  name: string | undefined;
  fd: string | undefined;
  td: string | undefined;
  enabled: boolean | undefined;
  surveyYaml: string | undefined;
}

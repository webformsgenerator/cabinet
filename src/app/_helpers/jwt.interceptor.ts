import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import {catchError, Observable, of, tap} from 'rxjs';

import { AuthenticationService } from '../_services';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log("Interceptor");
    // add authorization header with jwt token if available
    let accessToken = this.authenticationService.accessToken;
    if (this.authenticationService.isAuthenticated && accessToken) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${accessToken}`
        }
      });
    }

    return next.handle(request).pipe(
      tap(evt => {
        if (evt instanceof HttpResponse) {
        }
      })
      // ,catchError((err: any) => {
      //   if(err instanceof HttpErrorResponse && err.status == 403) {
      //     this.authenticationService.logout();
      //     console.log(HttpErrorResponse)
      //   }
      //   return of(err);
      // })
    );

  }
}

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from "../../environments/environment";
import {AuthenticationResponse} from "../_models";

@Injectable({providedIn: 'root'})
export class AuthenticationService {
//  private currentUserSubject: BehaviorSubject<User>;
  private authData: AuthenticationResponse;
//  public currentUser: Observable<User>;

  constructor(private http: HttpClient) {
    let accessToken = localStorage.getItem('accessToken') || '';
    let refreshToken = localStorage.getItem('refreshToken') || '';
    //let userJson = localStorage.getItem('currentUser') || '{}';
    //this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(userJson));
    //this.currentUser = this.currentUserSubject.asObservable();
    this.authData = new AuthenticationResponse(accessToken, refreshToken);
  }

  // public get currentUserValue(): User {
  //   return this.currentUserSubject.value;
  // }

  get accessToken(): string {
    return this.authData['accessToken'];
  }

  get refreshToken(): string {
    return this.authData['refreshToken'];
  }

  public get isAuthenticated(): boolean {
    // Получаем токен
    if (!this.accessToken) {
      return false
    }

    // Если токен есть, то парсим его
    let accessTokenDict = this.parseJwt(this.accessToken)

    // Проверяем наличие срока действия
    if (!('exp' in accessTokenDict)) {
      return false
    }

    let accessTokenExp = accessTokenDict['exp']

    // Got current timestamp and compare it with timestamp from access token
    let currTimeStamp = Date.now() / 1000

    // Если текущая дата меньше чем дата просрочки токена то возвращаем что аутентификация успешная, иначе неуспешная
    return currTimeStamp <= accessTokenExp;
  }

  login(email: string, password: string) {
    return this.http.post<AuthenticationResponse>(environment.apiBaseUrl + '/user/login', {email, password})
      .pipe(map(response => {
        // login successful if there's a jwt token in the response
        if (response['accessToken']) {
          this.setAuthData(response);
          this.localSetAuthVariables(this.accessToken, this.refreshToken);
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          //localStorage.setItem('currentUser', JSON.stringify(user['DATA']));
          //this.currentUserSubject.next(user['DATA']);
        }

      }));
  }

  setAuthData(authData: AuthenticationResponse) {
    this.authData = authData;
  }

  localSetAuthVariables(accessToken: string, refreshToken: string) {
    localStorage.setItem('accessToken', accessToken);
    localStorage.setItem('refreshToken', refreshToken);
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
    this.authData = new AuthenticationResponse("", "")
    //this.currentUserSubject.next(null);
  }

  // private atob(input: string) {
  //   const chars: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
  //   const str = String(input).replace(/=+$/, '');
  //   if (str.length % 4 == 1) {
  //     throw new Error("'atob' failed: The string to be decoded is not correctly encoded.");
  //   }
  //   for (
  //     // initialize result and counters
  //     var bc = 0, bs = 0, buffer, idx = 0, output = '';
  //     // get next character
  //     buffer = str.charAt(idx++);
  //     // character found in table? initialize bit storage and add its ascii value;
  //     ~buffer && (bs = bc % 4 ? bs * 64 + buffer : buffer,
  //       // and if not first of each 4 characters,
  //       // convert the first 8 bits to one ascii character
  //     bc++ % 4) ? output += String.fromCharCode(255 & bs >> (-2 * bc & 6)) : 0
  //   ) {
  //     // try to find character in table (0-63, not found => -1)
  //     buffer = chars.indexOf(buffer);
  //   }
  //   return output;
  // };

  parseJwt(token: string) {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
  };


}

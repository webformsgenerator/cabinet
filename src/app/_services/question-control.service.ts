import {Injectable} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {FormElementBase} from '../_components/dynamic-element/form-element-base';
import {SurveyBlock} from "../_models/surveyBlock";

@Injectable()
export class QuestionControlService {
  toFormGroup(questions: FormElementBase<string>[]) {
    const group: any = {};

    questions.forEach(question => {
      group[question.key] = question.required ? new FormControl(question.value || '', Validators.required)
        : new FormControl(question.value || '');
    });
    return new FormGroup(group);
  }

  /**
   * Из объекта в FormControl
   * @param surveyBlocks
   */
  toFormGroupFromSurveyBlocks(surveyBlocks: { [key: string]: SurveyBlock }) {
    let group: { [key: string]: FormControl } = {}

    for (let surveyBlocksKey in surveyBlocks) {
      let blockElements = surveyBlocks[surveyBlocksKey].getElementsOrdered()

      blockElements.forEach(element => {
        group[element.key] = element.required ? new FormControl(element.value || '', Validators.required)
          : new FormControl(element.value || '');
      });
    }

    return new FormGroup(group)
  }

  toFormGroupFromSurveyBlock(surveyBlock: SurveyBlock) {
    const group: any = {}

    let blockElements = surveyBlock.getElementsOrdered()

    blockElements.forEach(element => {
      group[element.key] = element.required ? new FormControl(element.value || '', Validators.required)
        : new FormControl(element.value || '');
    });

    return new FormGroup(group)
  }
}

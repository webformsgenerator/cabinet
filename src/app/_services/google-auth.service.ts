import {Injectable} from "@angular/core";
import {AuthenticationResponse} from "../_models";
import {environment} from "../../environments/environment";
import {map} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {AuthenticationService} from "./authentication.service";

@Injectable({
  providedIn: 'root'
})

export class GoogleAuthService {
  private user: gapi.auth2.GoogleUser | undefined;
  private error: any;
  private gapiSetup: boolean = false;
  private authInstance: gapi.auth2.GoogleAuth | undefined;

  constructor(private http: HttpClient, private authSrv: AuthenticationService) {
    this.initGoogleAuth();
  }

  async initGoogleAuth(): Promise<void> {
    //  Create a new Promise where the resolve
    // function is the callback passed to gapi.load
    const pload = new Promise((resolve) => {
      gapi.load('auth2', resolve);
    });

    // When the first promise resolves, it means we have gapi
    // loaded and that we can call gapi.init
    return pload.then(async () => {
      await gapi.auth2
        .init({client_id: '1094170618357-2cb6rqcu5qndrp5fceb3tqfujjm7drf5.apps.googleusercontent.com'})
        .then(auth => {
          this.gapiSetup = true;
          this.authInstance = auth;
        });
    });
  }

  signIn(): Promise<unknown> {
    return new Promise(async (resolve, reject) => {
      await this.checkGoogleAuth();
      if (!this.user) {
        throw "Error";
      }
      await this.login(this.user.getAuthResponse().id_token).subscribe(
        () => {
          resolve("Ok");
        }
      );
    });
    //return this.login(this.user.getAuthResponse().id_token);

    // return this.authInstance?.signIn().then(
    //   user => {
    //     //console.log(user.getAuthResponse().id_token);
    //     return this.login(user.getAuthResponse().id_token);
    //   }
    // ).catch(err => {
    //   console.log(err);
    // });
  }

  public signOut() {
    this.authInstance?.signOut();
  }

//   async authenticate(): Promise<unknown> {
//     // Initialize gapi if not done yet
//     if (!this.gapiSetup) {
//       await this.initGoogleAuth();
//     }
//
// // Resolve or reject signin Promise
//     return new Promise(async () => {
//       await this.authInstance.signIn().then(
//         user => this.user = user,
//         error => this.error = error);
//     });
//   }

  checkGoogleAuth(): Promise<unknown> {
    return new Promise(async (resolve, reject) => {
        await this.authInstance?.signIn().then(
          user => this.user = user,
          error => this.error = error
        );
        resolve("Ok");

      }
    );
  }

  login(token: string) {
    return this.http.post<AuthenticationResponse>(environment.apiBaseUrl + '/user/login_google', {token})
      .pipe(map(response => {
        // login successful if there's a jwt token in the response
        if (response['accessToken']) {
          this.authSrv.setAuthData(response);
          this.authSrv.localSetAuthVariables(this.authSrv.accessToken, this.authSrv.refreshToken);
        }
      }));
  }

}


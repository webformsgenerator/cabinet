import {Injectable} from '@angular/core';

import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {SurveyDTO} from "../_models";
import {Survey} from "../_models/survey";

@Injectable()
export class SurveyService {
  private apiServiceUrl = environment.apiBaseUrl;


  constructor(private http: HttpClient) {
  }

  getAll() {
    return this.http.get<any>(`${this.apiServiceUrl}/survey`);
  }

  addSurvey(surveyDTO: SurveyDTO) {
    return this.http.post<any>(`${this.apiServiceUrl}/survey`, surveyDTO);
  }

  updateSurvey(surveyDTO: SurveyDTO) {
    console.log("updateSurvey")
    return this.http.put<any>(`${this.apiServiceUrl}/survey/`, surveyDTO);
  }

  updateSurveyV3(surveyUUID: string | null, survey: Survey) {
    console.log("updateSurveyV3")
    return this.http.put<any>(`${this.apiServiceUrl}/survey/${surveyUUID}`, survey);
  }

  deleteSurvey(surveyId: number) {
    return this.http.delete<any>(`${this.apiServiceUrl}/survey/${surveyId}`);
  }

  /**
   * Get JSON data
   */
  getByUUID(surveyUUID: string | null) {
    return this.http.get<any>(`${this.apiServiceUrl}/survey/${surveyUUID}`);
  }

  getByID(surveyId: number) {
    return this.http.get<any>(`${this.apiServiceUrl}/survey/${surveyId}`);
  }
}

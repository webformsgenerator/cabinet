import { Routes } from '@angular/router';

import {HomeComponent} from './home';
import {SurveyComponent} from './_components/survey';
import {LoginComponent} from './login';
import {AuthGuard} from './_helpers';
import {PageNotFound} from "./_components/pagenotfound";
import {RegistrationComponent} from "./_components/registration";
import {FormEditorComponent} from "./form-editor/form-editor.component";

export const routes: Routes = [
  {path: '', component: HomeComponent, canActivate: [AuthGuard]},
  {path: 'survey', component: SurveyComponent, canActivate: [AuthGuard]},
  {path: 'survey/form-editor/:surveyUUID', component: FormEditorComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'registration', component: RegistrationComponent},
  {path: 'survey/play/:surveyId', component: SurveyComponent},
  {path: '**', component: PageNotFound}
];

// export const appRoutingModule = RouterModule.forRoot(routes);

import {ApplicationConfig, importProvidersFrom} from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {JwtInterceptor} from "./_helpers";
import {ErrorInterceptor} from "./_helpers/error.interceptor";
import {SurveyService} from "./_services/survey.service";


export const appConfig: ApplicationConfig = {
  providers: [provideRouter(routes)
    , importProvidersFrom(HttpClientModule)
    ,{ provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
    ,{ provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
    ,SurveyService
  ]
};

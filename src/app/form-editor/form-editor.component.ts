import {Component, OnInit} from '@angular/core';
import {CommonModule} from "@angular/common";
import {FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {FormElementBase} from "../_components/dynamic-element/form-element-base";
import {SurveyService} from "../_services/survey.service";
import {DynamicFormComponent} from "../_components/dynamicform/dynamic-form.component";
import {AsyncPipe} from '@angular/common';
import {QuestionControlService} from "../_services/question-control.service";
import {DynamicFormQuestionComponent} from "../_components/dynamic-element/dynamic-element.component";
import {ElementSelectNumRangeCondition} from "../_components/dynamic-element/element-select-num-range-condition";
import {ElementNumber} from "../_components/dynamic-element/element-number";
import {ElementText} from "../_components/dynamic-element/element-text";
import {ElementCheckbox} from "../_components/dynamic-element/element-checkbox";
import {ElementTextArea} from "../_components/dynamic-element/element-textarea";
import {Survey} from "../_models/survey";
import {SurveyBlock} from "../_models/surveyBlock";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {ActivatedRoute} from "@angular/router";
import {SurveyDTO} from "../_models";

@Component({
  selector: 'app-form-editor',
  standalone: true,
  providers: [QuestionControlService],
  imports: [
    AsyncPipe,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DynamicFormComponent,
    DynamicFormQuestionComponent],
  templateUrl: './form-editor.component.html',
  styleUrl: './form-editor.component.css'
})
export class FormEditorComponent implements OnInit {
  public survey!: Survey
  public currentBlockName: string = "";
  surveyUUID: string | null = "";

  //questions: FormElementBase<any>[]
  //questions$: Observable<FormElementBase<any>[]>;
  newElementForm: FormGroup;
  addonElements: SurveyBlock = new SurveyBlock()// FormElementBase<string>[] | null = []

  get addonElementsFormGroup() {
    return this.qcs.toFormGroupFromSurveyBlock(this.addonElements);
  }


  constructor(private formBuilder: FormBuilder, service: SurveyService, private qcs: QuestionControlService,
              private surveyService: SurveyService,
              private route: ActivatedRoute) {
    this.survey = new Survey()

    //this.questions = service.getQuestions();
    //this.questions$ = of(this.questions)

    //this.newElementForm = this.qcs.toFormGroup(this.questions as FormElementBase<string>[]);
    this.newElementForm = new FormGroup({
      elementType: new FormControl(),
      elementName: new FormControl(),
      elementInternalName: new FormControl(),
      elementIsRequired: new FormControl(),
    }, undefined, undefined)
  }

  ngOnInit() {
    this.newElementForm = this.formBuilder.group({
      elementType: ['', Validators.required],
      elementName: ['', Validators.required],
      elementInternalName: ['', Validators.required],
      elementIsRequired: ['', Validators.required],
    });

    // TODO: Check variable surveyUUID is set
    this.surveyUUID = this.route.snapshot.paramMap.get('surveyUUID')

    // Load survey data
    this.loadFormData(this.surveyUUID)
  }

  public addBlock() {
    this.survey.addBlock()
  }

  public updateCurrentBlockName(blockName: string) {
    this.currentBlockName = blockName;
  }

  public regenerateBlocks() {
    let blocksDiv = document.getElementById('blocksDiv');

    if (blocksDiv == null) {
      throw new Error("blocksDiv is not exists");
    }

    blocksDiv.innerHTML = "";

    // Получаем перечень ключей из словаря с блоками
    let blocksKeys = Object.keys(this.survey['blocks']);

    for (let i = 0; i < blocksKeys.length; i++) {
      let div = document.createElement('div');
      div.id = 'div_' + blocksKeys[i];
      div.innerText = blocksKeys[i];
      if (this.currentBlockName == blocksKeys[i]) {
        div.style.fontWeight = 'bold';
      }
      div.setAttribute("onclick", "updateCurrentBlockName('" + blocksKeys[i] + "');");
      blocksDiv.appendChild(div);
    }
  }

  public updateAdditionParams(select: string) {
    //console.log("updateAdditionParams")
    const elementType = select;
    //console.log(elementType)

    // Получаем экземпляр класса
    const elementObj: FormElementBase<any> = this.getElementObject(elementType)

    // Получаем массив доп. элементов которые нужно получить с формы
    let block = new SurveyBlock()

    let elements = elementObj.getAdditionElements()
    // Проходимся по массиву и добавляем доп. элементы
    for (let i = 0; i < elements.length; i++) {
      block.addElement(elements[i])
    }

    this.addonElements = block
  }

  /**
   * Добавление элемента в массив
   */
  public addElement() {
    console.log("addElement");

    // Проверяем что форма валидна
    if (!this.newElementForm.valid) {
      console.log("form is not valid");
      console.log(this.newElementForm)
      return;
    }

    // Получаем выбранный тип элемента
    const elementType = (document.getElementById('elementType') as HTMLInputElement).value;
    //console.log(elementType);

    // Получаем экземляр класса для данного типа элемента
    const elementObj: FormElementBase<any> = this.getElementObject(elementType);

    // Установка основных параметров
    elementObj.key = this.newElementForm.controls['elementInternalName'].value
    // elementObj.value = ''
    elementObj.label = this.newElementForm.controls['elementName'].value

    // Установка дополнительных параметров
    if (elementObj instanceof ElementTextArea) {
      elementObj.cols = Number((document.getElementById('textAreaCols') as HTMLInputElement).value)
      elementObj.rows = Number((document.getElementById('textAreaRows') as HTMLInputElement).value)
      // TODO: Как надо бы сделать:
      // elementObj.rows = this.newElementForm.controls['textAreaRows'].value
    }


    //console.log(elementObj);

    this.survey.blocks[this.currentBlockName].addElement(elementObj);

    console.log(this.survey);
  }

  private getElementObject(elementType: string): FormElementBase<any> {

    if (elementType === 'numberList') {
      return new ElementNumber({});
    } else if (elementType === 'stringList') {
      return new ElementText({});
    } else if (elementType === 'textList') {
      return new ElementTextArea({});
    } else if (elementType === 'checkBoxList') {
      return new ElementCheckbox({});
    } else if (elementType === 'geoCoordList') {
      return new ElementText({});
      //element = new StringElement();
    } else if (elementType === 'selectNumRangeConditionList') {
      return new ElementSelectNumRangeCondition({});
    } else {
      console.log('Unexpected type of element: ' + elementType);
      throw new Error('Unexpected type of element: ' + elementType);
    }
  }

  public save() {
    console.log(this.survey)
    this.surveyService.updateSurveyV3(this.surveyUUID, this.survey).subscribe(
      (response: SurveyDTO) => {
        console.log(response);
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    );
  }

  public loadFormData(surveyUUID: string | null){
    this.surveyService.getByUUID(surveyUUID).subscribe(
      (response: Survey) => {
        this.survey = new Survey(response)
        console.log(this.survey)
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    );
  }

}




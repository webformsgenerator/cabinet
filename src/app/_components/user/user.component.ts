import {Component} from '@angular/core';
import {User} from "../../_models";


@Component({
  selector: 'user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  standalone: true
})
export class UserComponent {
  public user: User | undefined;
}

// deleteUser(id: number) {
//   this.userService.delete(id).pipe(first()).subscribe(() => {
//     this.loadAllUsers()
//   });
// }


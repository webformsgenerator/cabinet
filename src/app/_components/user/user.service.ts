import { Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {AuthenticationResponse, User} from "../../_models";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private apiServiceUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {

  }

  public registration(user: User) {
    return this.http.post<AuthenticationResponse>(`${this.apiServiceUrl}/user/registration`, user);
  }

  getAll() {
    return this.http.get<User[]>(`${this.apiServiceUrl}/user/all`);
  }

  public getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${this.apiServiceUrl}/user/all`)
  }

  public addUser(user: User): Observable<User> {
    return this.http.post<User>(`${this.apiServiceUrl}/user`, user)
  }

  public updateUser(user: User): Observable<User> {
    return this.http.put<User>(`${this.apiServiceUrl}/user`, user)
  }

  public deleteUser(userId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServiceUrl}/user/${userId}`)
  }

  register(user: User) {
    return this.http.post(`${this.apiServiceUrl}/users/register`, user);
  }

  delete(id: number) {
    return this.http.delete(`${this.apiServiceUrl}/users/${id}`);
  }
}

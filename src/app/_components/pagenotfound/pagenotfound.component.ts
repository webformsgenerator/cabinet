import {Component, Injectable} from '@angular/core';


@Injectable({
  providedIn: 'root'
})

@Component({ selector: 'pagenotfound', standalone: true, templateUrl: 'pagenotfound.component.html' })
export class PageNotFound { }

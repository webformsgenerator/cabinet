import {Component, Input} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormGroup, ReactiveFormsModule} from '@angular/forms';

import {QuestionControlService} from '../../_services/question-control.service';
import {DynamicFormQuestionComponent} from "../dynamic-element/dynamic-element.component";
import {SurveyBlock} from "../../_models/surveyBlock";

@Component({
  standalone: true,
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  providers: [QuestionControlService],
  imports: [CommonModule, DynamicFormQuestionComponent, ReactiveFormsModule],
})
export class DynamicFormComponent {
  private _form!: FormGroup;

  @Input()
  blocks: { [key: string]: SurveyBlock } = {};

  get form() {
    //return this.qcs.toFormGroup(this.questions as FormElementBase<string>[]);
    return this.qcs.toFormGroupFromSurveyBlocks(this.blocks);
  }

  payLoad = '';

  constructor(private qcs: QuestionControlService) {
  }

  onSubmit() {
    this.payLoad = JSON.stringify(this.form.getRawValue());
  }
}

import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, FormsModule, Validators, ReactiveFormsModule, FormControl} from '@angular/forms';
import {first} from 'rxjs/operators';
import {UserService} from "../user";
import {AlertService} from "../alert/alert.service";
import {AuthenticationService} from "../../_services";
import {GoogleAuthService} from "../../_services/google-auth.service";
import {CommonModule} from "@angular/common";


@Component({
  selector: 'registration',
  standalone: true,
  templateUrl: 'registration.component.html',
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class RegistrationComponent implements OnInit {
  registrationForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private alertService: AlertService,
    private googleAuthService: GoogleAuthService
  ) {
    this.registrationForm = new FormGroup({
      firstName: new FormControl(),
      lastName: new FormControl(),
      email: new FormControl(),
      password: new FormControl(),
    }, undefined, undefined)
    // redirect to home if already logged in
    if (this.authenticationService.isAuthenticated) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.registrationForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registrationForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registrationForm.invalid) {
      return;
    }

    this.loading = true;
    this.userService.registration(this.registrationForm.value)
      .pipe(first())
      .subscribe(
        response => {
          if (response['accessToken']) {
            this.authenticationService.localSetAuthVariables(response['accessToken'], response['refreshToken']);
            this.router.navigate(['']);
          } else {
            this.alertService.error("Something wrong");
          }
          this.loading = false;
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  googleSignIn() {
    this.googleAuthService.signIn()
      .then(() => {
        this.loading = false;
        this.router.navigate(['/']);
      });
  }
}

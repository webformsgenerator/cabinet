import {Component, OnInit} from '@angular/core';
import {SurveyDTO} from "../../_models";
import {HttpErrorResponse} from "@angular/common/http";
import {FormsModule, NgForm, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {Router} from "@angular/router";
import {SurveyService} from "../../_services/survey.service";

@Component({
  selector: 'survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css'],
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class SurveyComponent implements OnInit {
  public surveyList: SurveyDTO[] | undefined;
  public updateSurvey: SurveyDTO = new SurveyDTO();
  public deleteSurvey: SurveyDTO = new SurveyDTO();

  constructor(private surveyService: SurveyService, private router: Router) {
  }

  ngOnInit(): void {
    this.getSurveyList();
  }

  public getSurveyList(): void {
    this.surveyService.getAll().subscribe(
      (response: any) => {
        //console.log(response)
        this.surveyList = response;
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    )
  }

  public onOpenModal(survey: SurveyDTO | null, mode: string): void {
    console.log("onOpenModal");
    const container = document.getElementById('main-container');

    if (!container) {
      throw new Error("Container main-container is not exist");
    }

    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      console.log('mode is add');
      button.setAttribute('data-target', '#addModal');
    }

    if (mode === 'update') {
      if (!survey) {
        throw new Error("user is not defined");
      }
      this.updateSurvey = survey;
      button.setAttribute('data-target', '#updateModal');
    }
    if (mode === 'delete') {
      if (!survey) {
        throw new Error("user is not defined");
      }
      this.deleteSurvey = survey;
      button.setAttribute('data-target', '#deleteModal');
    }
    container.appendChild(button);
    button.click();
  }

  public onAddSurvey(addForm: NgForm): void {
    let form = document.getElementById('add-form');

    if (!form) {
      throw new Error("form not found");
    }

    form.click();

    this.surveyService.addSurvey(addForm.value).subscribe(
      (response: SurveyDTO) => {
        console.log(response);
        this.getSurveyList();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    );
  }

  public onUpdateItem(survey: SurveyDTO): void {
    this.surveyService.updateSurvey(survey).subscribe(
      (response: SurveyDTO) => {
        console.log(response);
        this.getSurveyList();
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    );
  }

  public onDeleteItem(userId: number | undefined): void {
    if (!userId) {
      throw new Error("userId is not defined");
    }
    this.surveyService.deleteSurvey(userId).subscribe(
      (response: void) => {
        console.log(response);
        this.getSurveyList();
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    );
  }

  public onEditForm(surveyUUID: string | undefined): void {
    this.router.navigate([`/survey/form-editor/${surveyUUID}`]);
  }

}

// private loadAllUsers() {
//   this.userService.getAll().pipe(first()).subscribe(users => {
//     this.users = users;
//   });
// }

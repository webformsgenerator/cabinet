import { FormElementBase } from './form-element-base';

export class ElementText extends FormElementBase<string> {
  override controlType = 'text';
  override type = 'stringList'
}

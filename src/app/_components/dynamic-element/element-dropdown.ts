import { FormElementBase } from './form-element-base';

export class ElementDropdown extends FormElementBase<string> {
  override controlType = 'select';
  override type = 'dropDownList'

}

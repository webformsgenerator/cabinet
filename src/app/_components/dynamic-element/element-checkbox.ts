import { FormElementBase } from './form-element-base';

export class ElementCheckbox extends FormElementBase<string> {
  override controlType = 'checkbox';
  override type = 'checkBoxList'
}

export class FormElementBase<T> {
  value: T | undefined;
  key: string
  label: string
  type: string;
  defaultText: string
  required: boolean
  controlType?: string
  options: {key: string, value: string}[];

    constructor(options: {
    value?: T;
    key?: string;
    label?: string;
    required?: boolean;
    arrayName?: string;
    controlType?: string;
    type?: string;
    defaultText?: string;
    options?: {key: string, value: string}[];
  } = {}) {
    this.value = options.value;
    this.key = options.key || '';
    this.label = options.label || '';
    this.required = !!options.required;
    this.controlType = options.controlType || '';
    this.defaultText = options.type || '';
    this.type = options.type || '';
    this.options = options.options || [];
  }

  getAdditionElements(): FormElementBase<string>[] {
    return []
  }

  checkCompleteForm(skipAlert=false) {
    // const formRemark = [];
    // if ((document.getElementById('elementInternalName') as HTMLInputElement).value=='') {
    //   formRemark.push('Внутреннее имя должно быть заполнено.');
    // }
    //
    // if ((document.getElementById('elementName')as HTMLInputElement).value=='') {
    //   formRemark.push('Имя элемента должно быть заполнено.');
    // }
    //
    // if (!skipAlert && formRemark.length > 0) {
    //   alert(formRemark.join('\n'));
    //   return false
    // }
    //
    // if (skipAlert) {
    //   return formRemark;
    // }
    return true
  }

}

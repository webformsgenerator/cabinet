import {Component, Input, OnInit} from '@angular/core';
import {FormGroup, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {FormElementBase} from './form-element-base';
import {ElementTextArea} from "./element-textarea";

@Component({
  standalone: true,
  selector: 'app-question',
  templateUrl: './dynamic-element.component.html',
  imports: [CommonModule, ReactiveFormsModule],
})
export class DynamicFormQuestionComponent implements OnInit {
  textAreaElement!: ElementTextArea
  @Input() element!: FormElementBase<string>;
  @Input() form!: FormGroup;

  ngOnInit(): void {
    if (this.element instanceof ElementTextArea) {
      this.textAreaElement = this.element
    }
  }

  get isValid() {
    return this.form.controls[this.element.key].valid;
  }


}

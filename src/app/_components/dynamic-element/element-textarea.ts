import { FormElementBase } from './form-element-base';
import {ElementNumber} from "./element-number";

export class ElementTextArea extends FormElementBase<string> {
  override controlType = 'textarea';
  override type = 'textList'
  cols: number = 40
  rows: number = 5

  override getAdditionElements(): FormElementBase<string>[] {
    return [

      new ElementNumber ({
        key: 'textAreaCols',
        label: 'Ширина (количество символов в одной строке)',
        value: '',
        required: true,
      }),

      new ElementNumber ({
        key: 'textAreaRows',
        label: 'Количество строк',
        value: '',
        required: true,
      })

    ]

  }

}

import {FormElementBase} from './form-element-base';
import {ElementNumber} from "./element-number";
import {ElementText} from "./element-text";
import {ElementDropdown} from "./element-dropdown";

export class ElementSelectNumRangeCondition extends FormElementBase<string> {
  override controlType = 'selectNumRangeCondition';
  override type = 'selectNumRangeConditionList'

  override getAdditionElements(): FormElementBase<string>[] {
    return [

      new ElementNumber ({
        key: 'selectNumRangeConditionStartVal',
        label: 'Начальное значение',
        value: '',
        required: true,
      }),

      new ElementNumber ({
        key: 'selectNumRangeConditionEndVal',
        label: 'Конечное значение',
        value: '',
        required: true,
      }),

      new ElementDropdown({
        key: 'selectNumRangeConditionCondition',
        label: 'Условие сравнения',
        options: [
          {key: 'g',  value: 'Больше'},
          {key: 'ge',  value: 'Больше или равно'},
          {key: 'e',   value: 'Равно'},
          {key: 'le', value: 'Меньше или равно'},
          {key: 'l', value: 'Меньше'}
        ]
      }),

      new ElementNumber ({
        key: 'selectNumRangeConditionCompareVal',
        label: 'Сравниваемое значение',
        value: '',
        required: true,
      }),

      new ElementText({
        key: 'selectNumRangeConditionAction',
        label: 'Действие',
        value: '',
        required: true,
      })

    ]

  }
}

import { FormElementBase } from './form-element-base';

export class ElementNumber extends FormElementBase<string> {
  override controlType = 'number';
  override type = 'numberList'
}

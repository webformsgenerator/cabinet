import { Component } from '@angular/core';
import {Router, RouterLink, RouterOutlet} from "@angular/router";
import { AuthenticationService } from "./_services";
import { User } from "./_models";
//import { UserService } from "./_components/user";
import {AlertComponent} from "./_components/alert";
import {NgIf} from "@angular/common";
// import {NgIf} from "@angular/common";

@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  imports: [
    RouterLink,
    RouterOutlet,
    AlertComponent,
    NgIf,
    //NgIf
  ],
  //providers: [UserService],
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'cabinet';

  currentUser: User | undefined;

  public users: User[] | undefined;

  constructor(//private userService: UserService,
              private router: Router,
              public authenticationService: AuthenticationService
  ) {
    // this.authenticationService.currentUser.subscribe(
    //   x => (this.currentUser = x)
    // );
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(["/login"]);
  }
}

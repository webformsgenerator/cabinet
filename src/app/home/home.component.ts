import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { User } from '../_models';
import { AuthenticationService } from '../_services';

@Component({ templateUrl: 'home.component.html',
standalone: true})
export class HomeComponent implements OnDestroy {
  //currentUser: User = new User;
  //currentUserSubscription: Subscription;
  //users: User[] = [];

  constructor(
    private authenticationService: AuthenticationService,
    //private userService: UserService
  ) {
    // this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
    //   this.currentUser = user;
    // });
  }

  // ngOnInit() {
  //   //this.loadAllUsers();
  // }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    // this.currentUserSubscription.unsubscribe();
  }

}

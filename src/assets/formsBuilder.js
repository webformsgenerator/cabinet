var currentBlockName = ""
var survey = {name: "", about: "", mode: "", blocks:{} }

class BaseElement {
  labelString = ''
  nameString = ''
  defaultValue = ''
  required = false

  init(obj) {
    this.nameString = obj['name'];
    this.labelString = obj['label'];
  }

  initFromForm() {
    this.nameString = document.getElementById('elementInternalName').value;
    this.labelString = document.getElementById('elementName').value;
  }

  getAdditionElements() {
    return []
  }

  checkCompleteForm(skipAlert=false) {
    var formRemark = []
    if (document.getElementById('elementInternalName').value=='') {
      formRemark.push('Внутреннее имя должно быть заполнено.');
    }

    if (document.getElementById('elementName').value=='') {
      formRemark.push('Имя элемента должно быть заполнено.');
    }

    if (!skipAlert && formRemark.length > 0) {
      alert(formRemark.join('\n'));
      return false
    }

    if (skipAlert) {
      return formRemark;
    }
    return true


  }
}

class StringElement extends BaseElement {
  arrayName = 'stringList'

  getObject() {
    return {name: this.nameString, required: this.required, defaultValue: this.defaultValue, label: this.labelString}
  }

  getHtmlElements() {
    var newLabel = document.createElement('label');
    newLabel.for = this.nameString;
    newLabel.innerText = this.labelString;
    var newInput = document.createElement('input');
    newInput.type = 'text';
    newInput.id = this.nameString;
    newInput.name = this.nameString;
    newInput.class = 'form-control';

    return [newLabel, newInput];
  }
}

class NumberElement extends BaseElement {
  arrayName = 'numberList'
  labelString = ''
  nameString = ''
  defaultValue = ''
  required = false

  getObject() {
    return {name: this.nameString, required: this.required, defaultValue: this.defaultValue, label: this.labelString}
  }

  getHtmlElements() {
    var newLabel = document.createElement('label');
    newLabel.for = this.nameString;
    newLabel.innerText = this.labelString;
    var newInput = document.createElement('input');
    newInput.type = 'number';
    newInput.id = this.nameString;
    newInput.name = this.nameString;
    newInput.class = 'form-control';

    return [newLabel, newInput];
  }
}

class TextElement extends BaseElement {
  arrayName = 'textList'
  labelString = ''
  nameString = ''
  defaultValue = ''
  required = false

  getObject() {
    return {name: this.nameString, required: this.required, defaultValue: this.defaultValue, label: this.labelString}
  }

  getHtmlElements() {
    var newLabel = document.createElement('label');
    newLabel.for = this.nameString;
    newLabel.innerText = this.labelString;
    var newInput = document.createElement('textarea');
    newInput.id = this.nameString;
    newInput.name = this.nameString;
    newInput.class = 'form-control';
    newInput.cols=40
    newInput.rows=5

    return [newLabel, newInput];
  }
}

class CheckBoxElement extends BaseElement {
  arrayName = 'checkBoxList'
  labelString = ''
  nameString = ''
  defaultValue = ''
  required = false

  getObject() {
    return {name: this.nameString, required: this.required, defaultValue: this.defaultValue, label: this.labelString}
  }

  getHtmlElements() {
    var newLabel = document.createElement('label');
    newLabel.for = this.nameString;
    newLabel.innerText = this.labelString;
    var newInput = document.createElement('input');
    newInput.type = 'checkbox';
    newInput.id = this.nameString;
    newInput.name = this.nameString;
    newInput.class = 'form-control';

    return [newLabel, newInput];
  }
}

class SelectElement extends BaseElement {
  arrayName = 'selectList'
  labelString = ''
  nameString = ''
  defaultValue = ''
  required = false
  // Массив словарей [{val:'name1', text:'Вариант 1'}, {val:'name2', text:'Вариант 2'}]
  options = []

  init(obj) {
    super.init(obj)
    this.options = obj['options']
    this.required = obj['required']
  }

  getObject() {
    return {name: this.nameString, required: this.required, defaultValue: this.defaultValue, label: this.labelString, options: this.options}
  }

  getHtmlElements() {
    var newLabel = document.createElement('label');
    newLabel.for = this.nameString;
    newLabel.innerText = this.labelString;
    var newInput = document.createElement('select');
    newInput.id = this.nameString;
    newInput.name = this.nameString;
    newInput.class = 'form-control';

    // Добавляем options
    for (let i = 0; i < this.options.length; i++) {
      var opt = document.createElement('option');
      opt.value = this.options[i].val;
      opt.innerHTML = this.options[i].text;
      newInput.appendChild(opt);
    }

    return [newLabel, newInput];
  }
}

class selectNumRangeCondition extends BaseElement {
  arrayName = 'selectNumRangeConditionList'
  labelString = ''
  nameString = ''
  defaultValue = ''
  required = false

  startVal
  endVal
  condition = ""
  compareVal
  action = ""

  init(obj) {
    super.init(obj)
    this.startVal = obj['startVal'];
    this.endVal = obj['endVal'];
    this.condition = obj['condition'];
    this.compareVal = obj['compareVal'];
    this.action = obj['action'];
  }

  initFromForm() {
    super.initFromForm()

    this.startVal = document.getElementById('selectNumRangeConditionStartVal').value;
    this.endVal = document.getElementById('selectNumRangeConditionEndVal').value;
    this.condition = document.getElementById('selectNumRangeConditionCondition').value;
    this.compareVal = document.getElementById('selectNumRangeConditionCompareVal').value;
    this.action = document.getElementById('selectNumRangeConditionAction').value;
  }

  // Проверка формы что все обязательные значения заполнены
  checkCompleteForm(skipAlert=false) {
    var formRemark = super.checkCompleteForm(true)

    if (document.getElementById('selectNumRangeConditionStartVal').value=='') {
      formRemark.push('Начальное значение должно быть заполнено.');
    }

    if (document.getElementById('selectNumRangeConditionEndVal').value=='') {
      formRemark.push('Конечное значение должно быть заполнено.');
    }

    if (document.getElementById('selectNumRangeConditionCondition').value=='') {
      formRemark.push('Условие сравнения должно быть заполнено.');
    }

    if (document.getElementById('selectNumRangeConditionCompareVal').value=='') {
      formRemark.push('Сравниваемое значение должно быть заполнено.');
    }

    if (document.getElementById('selectNumRangeConditionAction').value=='') {
      formRemark.push('Действие должно быть заполнено.');
    }

    if (formRemark.length > 0) {
      alert(formRemark.join('\n'));
      return false
    }

    return true

  }

  getObject() {
    return {name: this.nameString, required: this.required, defaultValue: this.defaultValue,
      label: this.labelString, startVal: this.startVal, endVal: this.endVal, condition: this.condition,
      compareVal: this.compareVal, action: this.action}
  }

  getAdditionElements() {

    var elements = []
    const compareOptions = [{val:'g', text: 'Больше'}
      , {val:'ge', text: 'Больше или равно'}
      , {val:'ge', text: 'Равно'}
      , {val:'ge', text: 'Меньше или равно'}
      , {val:'ge', text: 'Меньше'}
    ]

    var items = new NumberElement()
    items.init({name: 'selectNumRangeConditionStartVal', label:'Начальное значение'})
    elements = elements.concat(items.getHtmlElements())
    elements.push(document.createElement('br'))

    items = new NumberElement()
    items.init({name: 'selectNumRangeConditionEndVal', label:'Конечное значение'})
    elements = elements.concat(items.getHtmlElements())
    elements.push(document.createElement('br'))

    items = new SelectElement()
    items.init({name: 'selectNumRangeConditionCondition', label:'Условие сравнения', options: compareOptions})
    elements = elements.concat(items.getHtmlElements())
    elements.push(document.createElement('br'))

    items = new NumberElement()
    items.init({name: 'selectNumRangeConditionCompareVal', label:'Сравниваемое значение'})
    elements = elements.concat(items.getHtmlElements())
    elements.push(document.createElement('br'))

    items = new StringElement()
    items.init({name: 'selectNumRangeConditionAction', label:'Действие'})
    elements = elements.concat(items.getHtmlElements())
    elements.push(document.createElement('br'))

    return elements
  }

  getHtmlElements() {

    var newLabel = document.createElement('label');
    newLabel.for = this.nameString;
    newLabel.innerText = this.labelString;
    var newInput = document.createElement('input');
    newInput.type = 'number';
    newInput.id = this.nameString;
    newInput.name = this.nameString;
    newInput.class = 'form-control';

    return [newLabel, newInput];
  }
}


function getElementObject(elementType) {
  switch (elementType) {
    case 'numberList':
      console.log('numberList');
      element = new NumberElement();
      break;
    case 'stringList':
      console.log('stringList');
      element = new StringElement();
      break;
    case 'textList':
      console.log('textList');
      element = new TextElement();
      break;
    case 'checkBoxList':
      console.log('checkBoxList');
      element = new CheckBoxElement();
      break;
    case 'geoCoordList':
      console.log('geoCoordList');
      element = new StringElement();
      break;
    case 'selectNumRangeConditionList':
      console.log('selectNumRangeConditionList');
      element = new selectNumRangeCondition();
      break;
    default:
      console.log('Unexpected type of element: ' + elementType);
      throw new Error('Unexpected type of element: ' + elementType);
  }

  return element;
}

function getElementObjectByName(block, elementName) {
  // Проходимся по всем типам
  // В каждом типе проверяем наличие имени
  // Если находим то возвращаем найденный элемент
  console.log('getElementObjectByName');
  console.log(block);
  console.log(elementName);

  elementTypes = ['numberList','stringList', 'textList', 'checkBoxList', 'geoCoordList', 'selectNumRangeConditionList']

  for (let et = 0; et < elementTypes.length; et++) {
    elementType = elementTypes[et]
    if (!(elementType in block)) continue;
    for (let i = 0; i < block[elementType].length; i++) {
      console.log(block[elementType][i]);
      if (block[elementType][i]['name'] == elementName) {
        element = getElementObject(elementType);
        element.init(block[elementType][i])
        return element;
      }
    }
  }

  throw new Error('Unknown element: ' + elementName);
}

function addElement() {
  // Получаем выбранный тип элемента
  var elementType = document.getElementById('elementType').value;
  console.log(elementType);

  // Получаем экземляр класса для данного типа элемента
  var elementObj = getElementObject(elementType);

  // Заполняем объект
  elementObj.initFromForm()

  // Проверяем что все обязательные поля заполнены
  var checkCompleteResult = elementObj.checkCompleteForm()
  if (!checkCompleteResult) {
    return
  }

  // Добавляем объект в выбранный блок
  // TODO - проверять currentBlockName что он установлен
  if (survey['blocks'][currentBlockName].hasOwnProperty(elementObj.arrayName)) {
    survey['blocks'][currentBlockName][elementObj.arrayName].push(elementObj.getObject());
  } else {
    console.log('else');
    survey['blocks'][currentBlockName][elementObj.arrayName] = [elementObj.getObject()];
  }

  // Добавляем элемент в список очередности элементов
  survey['blocks'][currentBlockName]['order'].push(elementObj.nameString);

  // Запускаем регенерацию формы
  regenerateForm();
}


function regenerateForm() {
  formSurveyDiv = document.getElementById('formSurveyDiv');
  formSurvey = document.createElement('form');
  formSurvey.id = 'formSurvey'

  // Очищаем форму если она есть
  document.getElementById('formSurvey')?.remove();

  // Проходимся по всем блокам
  blocksKeys = Object.keys(survey['blocks']);

  for (let i = 0; i < blocksKeys.length; i++) {
    // генерим div с блоком
    blockDiv = document.createElement('div');
    blockDiv.id = blocksKeys[i];

    // Накидываем элементы в div с блоком

    // Получаем перечень элементов в блоке
    elementsOrderList = survey['blocks'][blocksKeys[i]]['order'];

    // Проходимся по всем элементам в блоке массива
    for (let n = 0; n < elementsOrderList.length; n++) {
      // Накидываем элемент выбранного типа в форму

      // Получаем выбранный тип элемента
      elementObj = getElementObjectByName(survey['blocks'][blocksKeys[i]], elementsOrderList[n]);
      elements = elementObj.getHtmlElements();
      blockDiv.appendChild(elements[0]);
      blockDiv.appendChild(elements[1]);
      blockDiv.appendChild(document.createElement('br'));
    }

    // добавляем блок в форму
    formSurvey.appendChild(blockDiv);
  }

  // Добавлем форму в div
  formSurveyDiv.appendChild(formSurvey);
}

function addBlock() {
  block = {show: "true", order: [] };
  // Получаем количество элементов в списке
  blocksCnt = Object.keys(survey['blocks']).length;

  // Генерим очередное имя блоку
  blockName = 'block' + blocksCnt;

  // Добавляем блок под сгенерированным именем
  survey['blocks'][blockName] = block;

  // Запускаем регенерацию списка блоков
  regenerateBlocks();
}

function regenerateBlocks() {
  blocksDiv = document.getElementById('blocksDiv');
  blocksDiv.innerHTML = "";

  // Получаем перечень ключей из словаря с блоками
  blocksKeys = Object.keys(survey['blocks']);

  for (let i = 0; i < blocksKeys.length; i++) {
    div = document.createElement('div');
    div.id = 'div_' + blocksKeys[i];
    div.innerText = blocksKeys[i];
    if (currentBlockName == blocksKeys[i]) {
      div.style.fontWeight = 'bold';
    }
    div.setAttribute("onclick","updateCurrentBlockName('"+blocksKeys[i]+"');");
    blocksDiv.appendChild(div);
  }
}

function updateCurrentBlockName(blockName) {
  currentBlockName = blockName;
  regenerateBlocks();
}

function sendData() {
  var pathArray = window.location.pathname.split('/');
  var uuid = pathArray.slice(-1);

  var request = new XMLHttpRequest();

  request.onreadystatechange = function() {
    if (request.readyState === 4) {
      if (request.status === 200) {
        alert("Спасибо! Ваша форма успешно сохранена");
      } else {
        alert("Что-то пошло не так. Не удалось сохранить форму. Повторите пожалуйста попытку позднее");
      }
    }
  }

  request.open("POST", uuid);
  request.setRequestHeader("Content-Type", "application/json");
  request.send(JSON.stringify(survey));
}

function updateAdditionParams(select) {
  console.log("updateAdditionParams")
  var elementType = select.value
  console.log(select.value)

  // Получаем экземпляр класса
  elementObj = getElementObject(elementType)

  // Получаем массив доп. элементов которые нужно получить с формы
  addonElements = elementObj.getAdditionElements()
  console.log(addonElements)

  // Очищаем div
  document.getElementById('form1AdditionElements').innerHTML = "";

  // Проходимся по массиву и добавляем в див доп. элементы
  additionElementsDiv = document.getElementById('form1AdditionElements')
  for (let i = 0; i < addonElements.length; i++) {
    additionElementsDiv.appendChild(addonElements[i]);
  }
}
